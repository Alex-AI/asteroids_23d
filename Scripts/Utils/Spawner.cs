using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Spawner
{
    private ObjectsPool<GameObject> objectsPool;
    public SpawnArea2D spawnArea;
    private int objectsPerTimeDefault;

    public struct CustomReturnCallback
    {
        public System.Func<GameObject, string, bool> predicate;
        public System.Action<GameObject> action;
    }

    public List<CustomReturnCallback> customReturnCallbacks;

    public Spawner(GameObject gameObject, SpawnArea2D spawnArea, int objectsPerTime = 1)
    {
        objectsPool = new ObjectsPool<GameObject>(() => Object.Instantiate(gameObject));
        objectsPool.Return(gameObject);
        objectsPerTimeDefault = objectsPerTime;
        this.spawnArea = spawnArea;
    }

    public void AddCustomReturnCallback(System.Func<GameObject, string, bool> predicate, System.Action<GameObject> action)
    {
        if (customReturnCallbacks == null)
            customReturnCallbacks = new List<CustomReturnCallback>();
        CustomReturnCallback callback;
        callback.predicate = predicate;
        callback.action = action;
        customReturnCallbacks.Add(callback);
    }


    public void Spawn(Vector3 spawnPos, Vector3 destPos, int count)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject gameObject = objectsPool.Get();
            gameObject.GetComponent<EnemyBehaviour>().StartPosition(spawnPos);
            gameObject.GetComponent<EnemyBehaviour>().Destination(destPos);
            gameObject.GetComponent<EnemyBehaviour>().Spawner(this);
            gameObject.SetActive(true);
        }

    }

    /* Allow spawn area decide destination point */
    public void Spawn(Vector3 spawnPos, SpawnArea spawn, int count)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject gameObject = objectsPool.Get();
            gameObject.GetComponent<EnemyBehaviour>().StartPosition(spawnPos);
            if(spawn != null)
            {
                gameObject.GetComponent<EnemyBehaviour>().Destination(spawn.GetDestPoint());
            }
            
            gameObject.GetComponent<EnemyBehaviour>().Spawner(this);
            gameObject.SetActive(true);
        }

    }

    /* Allow spawn area decide spawn and destination point*/
    public void Spawn(SpawnArea spawn, int count)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject gameObject = objectsPool.Get();
            if (spawn != null)
            {
                spawn.GetRandomPoints(out Vector3 spawnPos, out Vector3 destPos);
                gameObject.GetComponent<EnemyBehaviour>().StartPosition(spawnPos);
                gameObject.GetComponent<EnemyBehaviour>().Destination(destPos);
            }

            gameObject.GetComponent<EnemyBehaviour>().Spawner(this);
            gameObject.SetActive(true);
        }

    }


    public IEnumerator Timer(SpawnArea spawn, float spawnTime)
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnTime);
            if(spawn != null)
            {
                spawn.GetRandomPoints(out Vector3 spawnPos, out Vector3 destPos);
                Spawn(spawnPos, destPos, objectsPerTimeDefault);
            }
            
        }
    }

    public bool Return(GameObject gameObject, string destroyedByTag)
    {
        if(gameObject == null) return false;

        gameObject.SetActive(false);
        objectsPool.Return(gameObject);

        if(customReturnCallbacks != null)
        {
            foreach (CustomReturnCallback item in customReturnCallbacks)
            {
                if (item.predicate(gameObject, destroyedByTag) == true)
                {
                    item.action(gameObject);
                }
            }
        }


        //if (child != null && destroyedByTag == ProjectileBehaviour.behaviourTag)
        //{
        //    Vector3 position = (child.spawnPosition != null) ? child.spawnPosition() : obj.transform.position;
        //    child.Spawn(position, Random.Range(1, child.objectsPerTimeDefault));
        //}

        return true;
    }

}
