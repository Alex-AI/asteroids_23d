using UnityEngine;

public abstract class SpawnPolygon {
    public const string tag = "Spawn";
    public const string layer = "Spawn";
    public GameObject colliderArea;
    public int edges;
    protected Vector3[] edgePosition;
    public Vector3 GetPoint(int index) => edgePosition[index % edges];
    public int RandomEdge() => Random.Range(0, edges);
    public virtual Vector3 GetRandomPoint()
    {
        return edgePosition[RandomEdge()];
    }

    public static void CreateSpawnBoundTrigger(Vector2 leftDown, Vector2 rightUp)
    {
        GameObject colliderArea = new GameObject("SpawnBoundTrigger");
        colliderArea.transform.position = Vector3.zero;
        BoxCollider collider = colliderArea.AddComponent<BoxCollider>();
        collider.isTrigger = true;
        colliderArea.gameObject.tag = tag;
        colliderArea.gameObject.layer = LayerMask.NameToLayer(tag);

        float xSize = Mathf.Abs((leftDown.x - rightUp.x)) * 1.1f;
        float ySize = Mathf.Abs((leftDown.y - rightUp.y)) * 1.1f;
        float zSize = 2;
        collider.size = new Vector3(xSize, ySize, zSize);
    }
}

public class SpawnRect : SpawnPolygon
{
    public SpawnRect(Vector2 leftDown, Vector2 rightUp)
    {
        edges = 4;
        edgePosition = new Vector3[edges];
        edgePosition[0] = leftDown;
        edgePosition[1] = new Vector2(leftDown.x, rightUp.y);
        edgePosition[2] = rightUp;
        edgePosition[3] = new Vector2(rightUp.x, leftDown.y);

        Debug.DrawLine(edgePosition[0], edgePosition[1], Color.red, 100000f);
        Debug.DrawLine(edgePosition[1], edgePosition[2], Color.red, 100000f);
        Debug.DrawLine(edgePosition[2], edgePosition[3], Color.red, 100000f);
        Debug.DrawLine(edgePosition[3], edgePosition[0], Color.red, 100000f);
    }

    public override Vector3 GetRandomPoint()
    {
        int edge = RandomEdge();
        return Vector3.Lerp(GetPoint(edge), GetPoint(edge + 1), Random.Range(0f, 1f));
    }
}

public class SpawnCube : SpawnPolygon
{
    public SpawnCube(Vector3 xyz0, Vector3 xyz1)
    {
        edges = 8;
        edgePosition = new Vector3[edges];
        /* Permutations with repetition, two 3 dimensional coordinates = 2^3 variations */
        edgePosition[0] = new Vector3(xyz0.x, xyz0.y, xyz0.z);
        edgePosition[1] = new Vector3(xyz0.x, xyz0.y, xyz1.z);
        edgePosition[2] = new Vector3(xyz1.x, xyz0.y, xyz1.z);
        edgePosition[3] = new Vector3(xyz1.x, xyz0.y, xyz0.z);
        edgePosition[4] = new Vector3(xyz0.x, xyz1.y, xyz0.z);
        edgePosition[5] = new Vector3(xyz0.x, xyz1.y, xyz1.z);
        edgePosition[6] = new Vector3(xyz1.x, xyz1.y, xyz1.z);
        edgePosition[7] = new Vector3(xyz1.x, xyz1.y, xyz0.z);
    }

}

public abstract class SpawnArea
{
    public SpawnPolygon spawnPolygon;
    public virtual Vector3 GetSpawnPoint()
    {
        return spawnPolygon.GetRandomPoint();
    }
    public virtual Vector3 GetDestPoint()
    {
        return spawnPolygon.GetRandomPoint();
    }
    public virtual Vector3 GetDestPoint(Vector3 currentPosition)
    {
        return GetDestPoint();
    }
    public abstract void GetRandomPoints(out Vector3 point_A, out Vector3 point_B);

}

public class SpawnArea2D : SpawnArea
{
    public SpawnArea2D(Vector2 leftDown, Vector2 rightUp)
    {
        spawnPolygon = new SpawnRect(leftDown, rightUp);
    }

    public override Vector3 GetDestPoint(Vector3 currentPosition)
    {
        /*TODO_1905: Find the way to improve dest point generation and get dest the opposite of current position */
        return base.GetDestPoint(currentPosition);
    }

    public override void GetRandomPoints(out Vector3 point_A, out Vector3 point_B)
    {
        int edge = spawnPolygon.RandomEdge();
        /* Lerp to get a random point on the specified side of rect, side is between point_A and point_B */
        point_A = Vector3.Lerp(spawnPolygon.GetPoint(edge), spawnPolygon.GetPoint(edge + 1), Random.Range(0f, 1f));
        point_B = Vector3.Lerp(spawnPolygon.GetPoint(edge + 2), spawnPolygon.GetPoint(edge + 3), Random.Range(0f, 1f));
    }

}

