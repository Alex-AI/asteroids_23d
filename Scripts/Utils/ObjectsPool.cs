
using System.Collections.Concurrent;
using System;

public class ObjectsPool<T>
{
    public readonly ConcurrentBag<T> pool;
    private readonly Func<T> fObjConstructor;   

    public ObjectsPool(Func<T> _fObjConstructor)
    {
        fObjConstructor = _fObjConstructor;
        pool = new ConcurrentBag<T>();
    }

    public T Get() => pool.TryTake(out T item) ? item : fObjConstructor();

    public void Return(T item) => pool.Add(item);
}
