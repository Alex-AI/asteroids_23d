using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBounds : MonoBehaviour
{
    private static LevelBounds instance;
    public const string levelBoundsTag = "Level";
    public const string levelBoundsLayer = "Level";
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            Create2DBounds();
        }
    }

    public static LevelBounds GetInstance()
    {
        return instance;
    }


    /* TODO_2406: Implement Create method with input parameters Vector3 xMax, Vector3 xMax */
    public void Create2DBounds()
    {
        GameObject sceneBounds = new GameObject("LevelBounds2D");
        sceneBounds.gameObject.tag = levelBoundsTag;
        sceneBounds.gameObject.layer = LayerMask.NameToLayer(levelBoundsLayer);

        ViewportBounds2D viewportBounds2D = new ViewportBounds2D(Camera.main, 0);

        Vector3 sizeHoriz = new Vector3(Mathf.Abs(viewportBounds2D.LeftDown().x - viewportBounds2D.RightDown().x), 0.1f, 1);
        Vector3 sizeVert = new Vector3(0.1f, Mathf.Abs(viewportBounds2D.LeftDown().y - viewportBounds2D.LeftUp().y), 1);

        BoxCollider sceneBoundsCollider = sceneBounds.AddComponent<BoxCollider>();
        sceneBoundsCollider.isTrigger = false;
        sceneBoundsCollider.size = sizeVert;
        sceneBoundsCollider.center = Vector3.Lerp(viewportBounds2D.LeftDown(), viewportBounds2D.LeftUp(), 0.5f);

        sceneBoundsCollider = sceneBounds.AddComponent<BoxCollider>();
        sceneBoundsCollider.isTrigger = false;
        sceneBoundsCollider.size = sizeHoriz;
        sceneBoundsCollider.center = Vector3.Lerp(viewportBounds2D.LeftUp(), viewportBounds2D.RightUp(), 0.5f);

        sceneBoundsCollider = sceneBounds.AddComponent<BoxCollider>();
        sceneBoundsCollider.isTrigger = false;
        sceneBoundsCollider.center = Vector3.Lerp(viewportBounds2D.RightUp(), viewportBounds2D.RightDown(), 0.5f);
        sceneBoundsCollider.size = sizeVert;

        sceneBoundsCollider = sceneBounds.AddComponent<BoxCollider>();
        sceneBoundsCollider.isTrigger = false;
        sceneBoundsCollider.center = Vector3.Lerp(viewportBounds2D.RightDown(), viewportBounds2D.LeftDown(), 0.5f);
        sceneBoundsCollider.size = sizeHoriz;

    }
}

public class ViewportBounds2D
{
    public Vector3 leftDown;
    public Vector3 leftUp;
    public Vector3 rightUp;
    public Vector3 rightDown;

    public ViewportBounds2D(Camera mainCamera, float offset = 0)
    {
        float cameraPosZ = mainCamera.transform.localPosition.z;

        leftDown = mainCamera.ViewportToWorldPoint(new Vector3(-offset, -offset, -cameraPosZ));
        rightUp = mainCamera.ViewportToWorldPoint(new Vector3(1 + offset, 1 + offset, -cameraPosZ));
        leftUp = new Vector2(leftDown.x, rightUp.y);
        rightDown = new Vector2(rightUp.x, leftDown.y);
    }

    /* TODO_2337: Create method to shift bounds by offset */
    public void ShiftBounds(float offset)
    {

    }

    public Vector3 LeftDown()
    {
        return leftDown;
    }
    public Vector3 LeftUp()
    {
        return leftUp;
    }
    public Vector3 RightUp()
    {

        return rightUp;
    }
    public Vector3 RightDown()
    {
        return rightDown;
    }

}

