using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    private static UIController instance;
    public GameObject mainMenuScreen;
    public GameObject playerHUDScreen;
    public GameObject deathScreen;

    private Text scoreText;
    private Slider laserCooldown;

    public void Awake()
    {
        if (instance == null)
        {
            instance = this;
            FindComponentsIfNeeded();
            DisableUI();
        }
    }

    private void FindComponentsIfNeeded()
    {
        if (mainMenuScreen == null || playerHUDScreen == null || deathScreen == null)
        {

            Component[] childItems = GetComponentsInChildren<Component>(true);
            foreach (Component item in childItems)
            {
                if (item.name.Contains("MainMenu"))
                {
                    mainMenuScreen = item.gameObject;
                }
                if (item.name.Contains("DeathScreen"))
                {
                    deathScreen = item.gameObject;
                }
                if (item.name.Contains("HUDScreen"))
                {
                    playerHUDScreen = item.gameObject;
                }
            }
        }
    }

    public void DisableUI()
    {
        mainMenuScreen.SetActive(false);
        playerHUDScreen.SetActive(false);
        deathScreen.SetActive(false);
    }

    public static UIController GetInstance()
    {
        return instance;
    }

    public void LoadMainMenu()
    {
        deathScreen.SetActive(false);
        playerHUDScreen.SetActive(false);

        mainMenuScreen.SetActive(true);

        if (gameObject.activeSelf)
            StopCoroutine(MainMenuEsc());

    }

    public void LoadHudScreen()
    {
        deathScreen.SetActive(false);
        mainMenuScreen.SetActive(false);
        

        playerHUDScreen.SetActive(true);
        if (gameObject.activeSelf)
            StartCoroutine(MainMenuEsc());
    }

    public void LoadDeathScreen()
    {
        
        mainMenuScreen.SetActive(false);
        playerHUDScreen.SetActive(false);

        deathScreen.SetActive(true);

        if (gameObject.activeSelf)
            StopCoroutine(MainMenuEsc());
    }

    public IEnumerator MainMenuEsc()
    {
        yield return new WaitUntil(PlayerInput.GetMenuInput);
        MainGame.GetInstance().LoadMenu();
    }


    public void UpdateScore(int value)
    {
        if(scoreText == null)
        {
            Component[] childItems = playerHUDScreen.GetComponentsInChildren<Component>(true);
            foreach (Component item in childItems)
            {
                if (item.name.Contains("Score"))
                {
                    scoreText = item.GetComponent<Text>();

                }
            }
        }

        scoreText.text = value.ToString();

    }

    public void PutLaserCooldown(float cooldownTime)
    {
        if (laserCooldown == null)
        {
            Component[] childItems = playerHUDScreen.GetComponentsInChildren<Component>(true);
            foreach (Component item in childItems)
            {
                if (item.name.Contains("CooldownSlider"))
                {
                    laserCooldown = item.GetComponent<Slider>();
                    
                }
            }
        }

        laserCooldown.maxValue = cooldownTime;
        laserCooldown.value = 0;
        StartCoroutine(LaserCooldownTimer(cooldownTime));
    }


    private IEnumerator LaserCooldownTimer(float cooldownTime)
    {
        if(laserCooldown != null)
        {
            laserCooldown.maxValue = Mathf.Abs(cooldownTime);
            laserCooldown.value = 0;
            while(laserCooldown.value < laserCooldown.maxValue)
            {
                yield return new WaitForFixedUpdate();
                laserCooldown.value += Time.deltaTime;
            }         
            
        }  
    }


}
