using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeathScreen : MonoBehaviour
{
    private void Awake()
    {
        Component[] buttons = GetComponentsInChildren<Button>(true);
        foreach (Button button in buttons)
        {
            if (button.name.Contains("PlayAgain"))
            {
                button.onClick.AddListener(PlayAgainButton);
            }

            if (button.name.Contains("Quit"))
            {
                button.onClick.AddListener(QuitButton);
            }

        }
    }

    public void PlayAgainButton()
    {
        MainGame.GetInstance().Restart();
    }


    public void QuitButton()
    {
        MainGame.GetInstance().Quit();
    }
}

