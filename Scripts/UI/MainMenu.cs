using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    private void Awake()
    {
        Component[] buttons = GetComponentsInChildren<Button>(true);
        foreach (Button button in buttons)
        {
            if(button.name.Contains("Play"))
            {
                button.onClick.AddListener(PlayButton);
            }

            if(button.name.Contains("Quit"))
            {
                button.onClick.AddListener(QuitButton);
            }

        }
    }

    public void PlayButton()
    {
        MainGame.GetInstance().StartPlaying();
    }
    

    public void QuitButton()
    {
        MainGame.GetInstance().Quit();
    }
}
