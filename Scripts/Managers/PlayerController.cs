using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private static PlayerController instance;
    public PlayerShip<ShipBehaviour> playerShip;
    public GameObject playerObject;
    private int score;

    private void Awake()
    {
        if(instance == null)
        {
            score = 0;
            instance = this;
            LoadPlayerShip();
        }

    }

    public void LoadPlayerShip()
    {
        if (RecourcesManager.IsReady())
        {
            playerShip = new PlayerShip<ShipBehaviour>("Ships", "Ship", false);
            playerObject = playerShip.prefab.gameObject;
        }

    }

    public static PlayerController GetInstance()
    {
        return instance;
    }

    public static void Enable()
    {
        GetInstance().playerObject.SetActive(true);
    }


    public static void Disable()
    {
        GetInstance().playerObject.SetActive(false);
    }

    public void GiveReward(int value)
    {
        score += value;
        UIController.GetInstance().UpdateScore(score);
    }
}
