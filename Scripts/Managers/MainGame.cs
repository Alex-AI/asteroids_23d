using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainGame : MonoBehaviour
{
    private static MainGame instance;


    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            RecourcesManager.LoadAssets(ViewManager.mode, "Asteroids", "Ships", "Projectiles", "UFOs");
            LoadControllers();
        }
    }

    private void Start()
    {
        LoadMenu();
    }

    public static MainGame GetInstance()
    {
        return instance;
    }


    private void LoadControllers()
    {
        gameObject.AddComponent<LevelBounds>();
        gameObject.AddComponent<PlayerController>();
        gameObject.AddComponent<EnemySpawnController>();
        gameObject.AddComponent<PlayerInput>();
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void StartPlaying()
    {
        PlayerController.Enable();
        UIController.GetInstance().LoadHudScreen();
        EnemySpawnController.GetInstance().StartSpawners();
    }

    public void LoadMenu()
    {
        StopPlaying();
        UIController.GetInstance().LoadMainMenu();
    }

    public void StopPlaying()
    {
        PlayerController.Disable();
        EnemySpawnController.GetInstance().StopSpawners();
    }

    public void EndGame()
    {
        StopPlaying();
        UIController.GetInstance().LoadDeathScreen();
    }

    public void Quit()
    {
        Debug.Log("Quit!");
        Application.Quit();
    }
}
