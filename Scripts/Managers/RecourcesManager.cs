using System.Collections.Generic;

public class RecourcesManager
{
    private static RecourcesManager instance;
    public List<Resource> storage;

    private RecourcesManager()
    {
        storage = new List<Resource>();
    }

    private RecourcesManager(Resource resource) : this()
    {
        storage.Add(resource);
    }

    public static bool IsReady()
    {
        return instance != null;
    }
    public static RecourcesManager Load(Resource resource)
    {
        if (GetInstance() == null)
        {
            instance = new RecourcesManager(resource);
        }
        else
        {
            Resource item = GetStorage(resource.folder);
            if(item != null)
            {
                GetInstance().storage.Remove(item);
            }
            GetInstance().storage.Add(resource);
        }
        
        return instance;
    }

    public static RecourcesManager GetInstance()
    {
        return instance;
    }

    public static Resource GetStorage(string folder)
    {
        if (instance != null)
        {
            return instance.storage.Find(resource => resource.folder.Contains(folder));
        }
        return null;

    }

    public static void LoadAssets(ViewManager.Mode mode, params string[] folders)
    {
        foreach (string folder in folders)
        {
            if (mode == ViewManager.Mode._2D)
            {
                Load(new Resource2D(folder));
            }
            else
            {
                Load(new Resource3D(folder));
            }

        }
    }
}

