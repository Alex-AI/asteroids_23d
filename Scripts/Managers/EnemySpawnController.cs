using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnController : MonoBehaviour
{
    private static EnemySpawnController instance;
    ViewportBounds2D viewportBounds2D;
    SpawnArea2D spawnArea;

    AsteroidEnemy<AsteroidBehaviour> largeAsteroid;
    AsteroidEnemy<AsteroidBehaviour> smallAsteroid;
    UFOEnemy<UFOBehaviour> ufoEnemy;


    Spawner largeAsteroidsSpawner;
    Spawner smallAsteroidSpawner;
    Spawner ufoSpawner;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            InitializeSpawnArea(Camera.main);
            InitializeEnemiesPrefabs();
            InitializeSpawners();
        }
    }

    public static EnemySpawnController GetInstance()
    {
        return instance;
    }

    public void InitializeSpawnArea(Camera mainCamera)
    {
        viewportBounds2D = new ViewportBounds2D(mainCamera, 0.2f);
        spawnArea = new SpawnArea2D(viewportBounds2D.LeftDown(), viewportBounds2D.RightUp());

        viewportBounds2D = new ViewportBounds2D(mainCamera, 0.4f); /* TODO_2337: Create method to shift bounds by offset */
        SpawnPolygon.CreateSpawnBoundTrigger(viewportBounds2D.leftDown, viewportBounds2D.rightUp);
    }

    public void InitializeEnemiesPrefabs()
    {
        if (RecourcesManager.IsReady())
        {
            largeAsteroid = new AsteroidEnemy<AsteroidBehaviour>(
                "Asteroids", "Large", AsteroidBehaviour.Size.Large);

            smallAsteroid = new AsteroidEnemy<AsteroidBehaviour>(
               "Asteroids", "Small", AsteroidBehaviour.Size.Small);

            ufoEnemy = new UFOEnemy<UFOBehaviour>(
                "UFOs", "UFO", PlayerController.GetInstance().playerObject);

            Meshes3DModeSizeFix();
        }
    }

    public void InitializeSpawners()
    {
        largeAsteroidsSpawner = new Spawner(largeAsteroid.prefab.gameObject, spawnArea);
        largeAsteroidsSpawner.AddCustomReturnCallback(IsDestroyedByPlayer, SpawnSmallAsteroids);

        smallAsteroidSpawner = new Spawner(smallAsteroid.prefab.gameObject, spawnArea);
        ufoSpawner = new Spawner(ufoEnemy.prefab.gameObject, spawnArea);
    }

    public void StartSpawners()
    {

        StartCoroutine(largeAsteroidsSpawner.Timer(spawnArea, 3f));
        StartCoroutine(ufoSpawner.Timer(spawnArea, 10f));
    }

    public void StopSpawners()
    {
        StopAllCoroutines();
    }

    /* If large asteroid was destroyed by player weapon */
    public static bool IsDestroyedByPlayer(GameObject asteroid, string destroyedByTag)
    {
        if(asteroid.TryGetComponent(out AsteroidBehaviour asteroidBehaviour) == true)
        {
            if(asteroidBehaviour.GetSize() == "Large")
            {
                return PlayerController.GetInstance().playerObject.CompareTag(destroyedByTag);
            }
        }
        return false;
    }

    public void SpawnSmallAsteroids(GameObject largeAsteroid)
    {
        smallAsteroidSpawner.Spawn(largeAsteroid.transform.position, spawnArea, Random.Range(2, 5));
    }


    /* Because of need to find or create suitable meshes for 3D mode*/
    public void Meshes3DModeSizeFix()
    {
        if(ViewManager.mode == ViewManager.Mode._3D)
        {
            PlayerController.GetInstance().playerObject.GetComponent<WeaponKinetic>().projectile.transform.localScale = new Vector3(0.04f, 0.04f, 0.04f);
            ufoEnemy.prefab.SetScale(new Vector3(3, 3, 3));
            largeAsteroid.prefab.SetScale(new Vector3(0.1f, 0.1f, 0.1f));
            smallAsteroid.prefab.SetScale(new Vector3(0.08f, 0.08f, 0.08f));
        }
    }

}
