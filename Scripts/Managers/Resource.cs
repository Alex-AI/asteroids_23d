using System.Collections.Generic;
using UnityEngine;

public class Resource
{
    public string folder;
    public Resource(string folder)
    {
        this.folder = folder;
    }

    public virtual Sprite GetSprite(string name) { return default; }
    public virtual Mesh GetMesh(string name) { return default;  }
    public virtual Material GetMaterial(string name) { return default; }
}

public class Resource2D : Resource
{
    private const string basePath = "2D/";
    public List<Sprite> spritesStorage;

    public Resource2D(string folder) : base(folder)
    {
        spritesStorage = new List<Sprite>(Resources.LoadAll<Sprite>(basePath + folder));
    }

    public override Sprite GetSprite(string name)
    {
        return spritesStorage.Find(x => x.name.Contains(name));
    }
}

public class Resource3D : Resource
{
    private const string basePath = "3D/";
    public List<Mesh> meshStorage;
    public List<Material> materialStorage;

    public Resource3D(string folder) : base(folder)
    {
        meshStorage = new List<Mesh>(Resources.LoadAll<Mesh>(basePath + folder));
        materialStorage = new List<Material>(Resources.LoadAll<Material>(basePath + folder));
    }

    public override Mesh GetMesh(string name)
    {
        return meshStorage.Find(x => x.name.Contains(name));
    }

    public override Material GetMaterial(string name)
    {
        return materialStorage.Find(x => x.name.Contains(name));
    }
}

