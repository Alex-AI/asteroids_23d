using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ViewManager
{

    public enum Mode { _2D, _3D };
    public static Mode mode = Mode._2D;


    public static void ChangeViewMode(Mode viewMode)
    {
        mode = viewMode;
        RecourcesManager.LoadAssets(viewMode, "Asteroids", "Ships", "Projectiles", "UFOs");
    }


}
