using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public static float GetHorizontalInput()
    {
        return Input.GetAxis("Horizontal");
    }

    public static float GetVerticalInput()
    {
        return Input.GetAxis("Vertical");
    }

    public static bool GetAttackInput()
    {
        return Input.GetKey(KeyCode.Space);
    }

    public static bool GetLaserInput()
    {
        return Input.GetKey(KeyCode.X);
    }

    public static bool GetMenuInput()
    {
        return Input.GetKey(KeyCode.Escape);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            ViewManager.ChangeViewMode(ViewManager.Mode._3D);
            MainGame.GetInstance().Restart();
        }
        else if (Input.GetKeyDown(KeyCode.T))
        {
            ViewManager.ChangeViewMode(ViewManager.Mode._2D);
            MainGame.GetInstance().Restart();

        }
    }

}
