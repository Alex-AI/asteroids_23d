using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameToken<T> where T : TokenBehaviour
{
    public ObjectPrefab prefab;
    public T behaviour;
    public GameToken(string resourecesFolder, string spriteName)
    {
        if (ViewManager.mode == ViewManager.Mode._2D)
        {
            prefab = new ObjectRendered2D(resourecesFolder, spriteName);
        }
        else
        {
            prefab = new ObjectRendered3D(resourecesFolder, spriteName);
        }
        behaviour = prefab.gameObject.AddComponent<T>();

    }

}


public class AsteroidEnemy<T> : GameToken<T> where T : TokenBehaviour
{
    public AsteroidEnemy(string resourecesFolder, string spriteName, AsteroidBehaviour.Size size)
        : base(resourecesFolder, spriteName)
    {
        AsteroidBehaviour asteroidBehaviour = prefab.gameObject.GetComponent<AsteroidBehaviour>();
        prefab.SetSortingLayer("Tokens");
        asteroidBehaviour.SetBehaviourLayer("Enemy");
        asteroidBehaviour.SetBehaviourTag("Enemy");
        asteroidBehaviour.SetSize(size);
    }
}

public class UFOEnemy<T> : GameToken<T> where T : TokenBehaviour
{
    public UFOEnemy(string resourecesFolder, string spriteName, GameObject target)
        : base(resourecesFolder, spriteName)
    {
        UFOBehaviour ufoBehaviour = prefab.gameObject.GetComponent<UFOBehaviour>();
        prefab.SetSortingLayer("Tokens");
        ufoBehaviour.SetBehaviourLayer("Enemy");
        ufoBehaviour.SetBehaviourTag("Enemy");
    }
}


public class PlayerShip<T> : GameToken<T> where T : TokenBehaviour
{
    public PlayerShip(string resourecesFolder, string spriteName, bool isActive)
        : base(resourecesFolder, spriteName)
    {
        ShipBehaviour shipBehaviour = prefab.gameObject.GetComponent<ShipBehaviour>();
        shipBehaviour.gameObject.SetActive(isActive);

        /* Adding kinetic weapon */
        WeaponKinetic weaponKinetic = prefab.gameObject.AddComponent<WeaponKinetic>();
        weaponKinetic.tag = "PlayerWeapon";
        weaponKinetic.muzzle = new GameObject("PlayerShipMuzzle");
        weaponKinetic.muzzle.SetActive(false);
        weaponKinetic.muzzle.transform.parent = prefab.gameObject.transform;
        weaponKinetic.muzzle.transform.position = prefab.gameObject.transform.position;
        GameToken<ProjectileBehaviour> projectile = new KineticBullet<ProjectileBehaviour>("Projectiles", "Bullet", weaponKinetic);
        weaponKinetic.projectile = projectile.prefab.gameObject;

        /* Adding laser */
        WeaponLaser weaponLaser = prefab.gameObject.AddComponent<WeaponLaser>();
        weaponLaser.muzzle = new GameObject("PlayerShipMuzzle");
        weaponLaser.muzzle.SetActive(false);
        weaponLaser.muzzle.transform.parent = prefab.gameObject.transform;
        weaponLaser.muzzle.transform.position = prefab.gameObject.transform.position;
        weaponLaser.tag = "PlayerWeapon";


        prefab.SetSortingLayer("Tokens");
        shipBehaviour.SetBehaviourLayer("Player");
        shipBehaviour.SetBehaviourTag("Player");
    }
}

public class KineticBullet<T> : GameToken<T> where T : TokenBehaviour
{
    public KineticBullet(string resourecesFolder, string spriteName, Weapon weapon)
        : base(resourecesFolder, spriteName)
    {
        ProjectileBehaviour projectileBehaviour = prefab.gameObject.GetComponent<ProjectileBehaviour>();
        projectileBehaviour.gameObject.SetActive(false);
        projectileBehaviour.gameObject.transform.position = weapon.muzzle.transform.position;
        projectileBehaviour.ownerTag = weapon.gameObject.tag;
        prefab.SetSortingLayer("Tokens");
        projectileBehaviour.SetBehaviourLayer("PlayerWeapon");
        projectileBehaviour.SetBehaviourTag("Player");

    }
}
