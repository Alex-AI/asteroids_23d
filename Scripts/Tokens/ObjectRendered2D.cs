using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRendered2D : ObjectPrefab
{
    public Sprite sprite;
    public SphereCollider collider;
    public SpriteRenderer renderer;

    public ObjectRendered2D(string resourecesFolder, string spriteName, bool addCollider = true) : base(resourecesFolder)
    {
        gameObject.name += spriteName;

        sprite = RecourcesManager.GetStorage(resourecesFolder).GetSprite(spriteName);
        renderer = gameObject.AddComponent<SpriteRenderer>();
        renderer.sprite = sprite;

        if (addCollider)
        {
            collider = gameObject.AddComponent<SphereCollider>();
            float raduis = Mathf.Abs((sprite.bounds.max.x + sprite.bounds.max.y)) / 2;
            collider.radius = raduis * 0.5f;
        }

    }

    public override void SetSortingLayer(string sortingLayer)
    {
        renderer.sortingLayerName = sortingLayer;
    }

}
