using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRendered3D : ObjectPrefab
{
    public Mesh mesh;
    public MeshFilter meshFilter;
    public MeshRenderer renderer;
    public Material material;
    public MeshCollider collider;
    public ObjectRendered3D(string resourecesFolder, string meshName, bool addCollider = true) : base(resourecesFolder)
    {
        gameObject.name += meshName;

        mesh = RecourcesManager.GetStorage(resourecesFolder).GetMesh(meshName);
        material = RecourcesManager.GetStorage(resourecesFolder).GetMaterial(meshName);
        meshFilter = gameObject.AddComponent<MeshFilter>();
        meshFilter.mesh = mesh;
        renderer = gameObject.AddComponent<MeshRenderer>();
        renderer.material = material;

        if (addCollider)
        {
            collider = gameObject.AddComponent<MeshCollider>();
            collider.sharedMesh = mesh;
            collider.convex = true;
        }
    }

    public override void SetSortingLayer(string sortingLayer)
    {
        renderer.sortingLayerName = sortingLayer;
    }
}
