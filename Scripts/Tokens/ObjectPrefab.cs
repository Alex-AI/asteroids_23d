using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Base class to create objects for the game */
public abstract class ObjectPrefab
{
    public GameObject gameObject;

    public GameObject GetGameObject()
    {
        return gameObject;
    }

    public ObjectPrefab(string resourecesFolder, bool isActive = false)
    {
        gameObject = new GameObject(resourecesFolder);
        gameObject.SetActive(isActive);
    }

    public ObjectPrefab(ObjectPrefab prefab, bool isActive = false)
    {
        gameObject = Object.Instantiate(prefab.gameObject);
        gameObject.SetActive(isActive);
    }

    public void SetScale(Vector3 scale)
    {
        gameObject.transform.localScale = scale;
    }

    public abstract void SetSortingLayer(string sortingLayer);

}
