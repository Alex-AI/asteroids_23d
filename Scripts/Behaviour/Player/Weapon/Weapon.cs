using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public GameObject muzzle;

    /* muzzlePosition should be parent to owner transform */
    public void SetMuzzlePosition(GameObject muzzle)
    {
        this.muzzle = muzzle;
    }

    protected virtual void MakeOneShot()
    {
        if (muzzle == null) Debug.LogError("Muzzle is null");
    }

}