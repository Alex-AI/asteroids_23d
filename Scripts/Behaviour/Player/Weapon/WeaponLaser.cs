using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponLaser : Weapon
{
    public float cooldownTime = 5f;
    private bool isReady;
    public float radius = 0.25f;

    private void OnEnable()
    {
        StartCoroutine(StartCooldown());
        StartCoroutine(Shooting());
    }


    protected override void MakeOneShot()
    {
        base.MakeOneShot();

        RaycastHit[] hitInfo = Physics.SphereCastAll(muzzle.transform.position, radius, transform.up);
        foreach (RaycastHit hit in hitInfo)
        {
            if (hit.collider.CompareTag("Enemy"))
            {
                if (hit.collider.gameObject.TryGetComponent(out EnemyBehaviour enemy))
                {
                    if( enemy.ReturnToSpawner(gameObject.tag) == false)
                    {
                        Destroy(hit.collider.gameObject);
                    }
                }
            }
        }

    }

    private IEnumerator StartCooldown()
    {
        isReady = false;
        UIController.GetInstance().PutLaserCooldown(cooldownTime);
        yield return new WaitForSeconds(cooldownTime);
        isReady = true;
    }


    public IEnumerator Shooting()
    {
        while (true)
        {
            yield return new WaitUntil(() => PlayerInput.GetLaserInput());
            if(isReady)
            {
                MakeOneShot();
                StartCoroutine(StartCooldown());
            }
        }
    }

}