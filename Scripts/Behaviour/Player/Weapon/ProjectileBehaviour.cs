using UnityEngine;

public class ProjectileBehaviour : TokenBehaviour
{
    public float speed = 2f;
    public string ownerTag = "";

    private void Awake()
    {
        if (rbody != null)  rbody.isKinematic = true;
    }

    private void FixedUpdate()
    {
        transform.position += transform.up * speed * Time.deltaTime;
    }


    private void OnCollisionEnter(Collision collision)
    {
        //if(collision.gameObject.tag != "Enemy")
            Destroy(gameObject);
    }


    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

}
