using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponKinetic : Weapon
{
    public GameObject projectile;
    public float attackSpeed = 0.5f;

    private void OnEnable()
    {
        StartCoroutine(Shooting());
    }

    protected override void MakeOneShot()
    {
        base.MakeOneShot();
        if (projectile != null && muzzle != null)
        {
            GameObject newProjectile = Instantiate(projectile, muzzle.transform.position, gameObject.transform.rotation);
            newProjectile.gameObject.SetActive(true);
        }
    }

    public IEnumerator Shooting()
    {
        while (true)
        {
            yield return new WaitUntil(() => PlayerInput.GetAttackInput());
            MakeOneShot();
            yield return new WaitForSeconds(attackSpeed);
        }
    }

}

