using UnityEngine;

public class ShipBehaviour : TokenBehaviour
{
    private float yawInput;
    private float thrustInput;

    public float thrust = 50f;
    public float thrustDeceleration = 2f;
    public float maxVelocity = 0.5f;

    public float torque = 30f;
    public float angularDeceleration = 8f;
    public float minAngularVelocity = 1f;
    public float speedAngularVelocityFactor = 1.5f;

    private void OnEnable()
    {
        Debug.Log("OnEnable");
        transform.position = Vector3.zero;
    }


    private void Update()
    {
        yawInput = PlayerInput.GetHorizontalInput();
        thrustInput = PlayerInput.GetVerticalInput();
    }

    private void FixedUpdate()
    {
        Move(Time.deltaTime);
        Turn(Time.deltaTime);
    }

    private void Move(float deltaTime)
    {

        if (thrustInput > 0)
        {
            rbody.AddForce(thrustInput * transform.up * thrust * deltaTime);
            rbody.velocity = Vector3.ClampMagnitude(rbody.velocity, maxVelocity);
        }
        else
        {
            rbody.velocity = Vector3.Lerp(rbody.velocity, Vector3.zero, thrustDeceleration * deltaTime);
        }
    }

    private void Turn(float deltaTime)
    {
        float angularVelocityZ;
        if (yawInput != 0)
        {
            float torqueZ = -1f * yawInput * torque * Mathf.Deg2Rad * deltaTime;
            rbody.AddTorque(new Vector3(0f, 0f, torqueZ), ForceMode.Impulse);
        }
        else
        {
            angularVelocityZ = Mathf.Lerp(rbody.angularVelocity.z, 0f, angularDeceleration * deltaTime);
            rbody.angularVelocity = new Vector3(0f, 0f, angularVelocityZ);
        }

        rbody.maxAngularVelocity = minAngularVelocity + speedAngularVelocityFactor * Mathf.Sin(rbody.velocity.magnitude/ maxVelocity * Mathf.PI);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("Collision with enemy - lose");

            MainGame.GetInstance().EndGame();
        }
            
    }

}
