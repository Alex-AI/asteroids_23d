using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class TokenBehaviour : MonoBehaviour
{
    public Rigidbody rbody;

    protected virtual void Start()
    {
        if(rbody == null)
        {
            rbody = GetComponent<Rigidbody>();
        }
        
        if (rbody != null)
        {
            rbody.useGravity = false;
            rbody.constraints = RigidbodyConstraints.FreezePositionZ |
                RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
        }
    }

    public void SetBehaviourTag(string behaviourTag)
    {
        gameObject.tag = behaviourTag;

    }

    public void SetBehaviourLayer(string behaviourLayer)
    {
        gameObject.layer = LayerMask.NameToLayer(behaviourLayer);
    }

    public static Vector2 LinearExtrapolate(Vector2 a, Vector2 b, float t)
    {
        if(b.x != a.x)
        {
            float cx = b.x + (b.x - a.x) * t;
            float cy = a.y + (b.y - a.y) / (b.x - a.x) * (cx - a.x);
            return new Vector2(cx, cy);
        }
        else
        {
            return new Vector2(a.x, b.y + (b.y - a.y)*t);
        }

    }
}
