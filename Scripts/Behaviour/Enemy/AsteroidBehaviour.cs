using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidBehaviour : EnemyBehaviour
{
    public float minSpeed = 0.1f;
    public float maxSpeed = 0.2f;
    public float speedFactor = 6f;
    private Vector3 destination;
    public enum Size { Large, Small };
    private Size size = Size.Large;
    public static string SizeToString(Size value) { return (value == Size.Large) ? "Large" : "Small"; }


    private void Awake()
    {
        reward = (size == Size.Large) ? 1 : 2;
    }

    private void OnEnable()
    {
        float randomSpeed = Random.Range(minSpeed, maxSpeed);
        currentSpeed = (size == Size.Small) ? speedFactor * randomSpeed : randomSpeed;
    }

    private void OnDisable()
    {
        currentSpeed = 0;
    }


    public void SetSize(Size value)
    {
        size = value;
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        rbody.angularVelocity = new Vector3(0,0, Random.Range(0.1f, 0.2f));
    }

    public string GetSize()
    {
        return SizeToString(size);
    }


    public override void Destination(Vector3 targetPoint)
    {
        destination = LinearExtrapolate(transform.position, targetPoint, 2);
        Debug.DrawLine(transform.position, targetPoint, Color.green, 5f);
        Debug.DrawLine(targetPoint, destination, Color.blue, 5f);
    }

    protected override Vector3 MovementVector()
    {
        return destination - transform.position;
    }
}
