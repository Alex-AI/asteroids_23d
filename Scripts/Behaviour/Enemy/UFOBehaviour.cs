using UnityEngine;

public class UFOBehaviour : EnemyBehaviour
{
    public float minSpeed = 0.06f;
    public float maxSpeed = 0.09f;
    private GameObject trackedObject;

    private void Awake()
    {
        reward = 3;
    }

    private void OnEnable()
    {
        currentSpeed = Random.Range(minSpeed, maxSpeed);
        trackedObject = PlayerController.GetInstance().playerShip.prefab.gameObject;
    }

    protected override Vector3 MovementVector()
    {
        //if (trackedObject == null &&  PlayerController.GetInstance().gameObject != null) 
        trackedObject = PlayerController.GetInstance().playerShip.prefab.gameObject;
        return trackedObject.transform.position - gameObject.transform.position;
    }
}

