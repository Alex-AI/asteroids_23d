using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EnemyBehaviour : TokenBehaviour
{
    protected Spawner spawner;
    protected float currentSpeed;
    protected int reward;
    public float invisibleLifetime = 5;

    protected abstract Vector3 MovementVector();

    protected virtual void FixedUpdate()
    {
        //rbody.velocity = (destination - transform.position).normalized * currentSpeed * Time.deltaTime;
        transform.position += MovementVector().normalized * currentSpeed * Time.deltaTime;
    }

    public virtual void Destination(Vector3 targetPoint) { }

    public void StartPosition(Vector3 startPosition)
    {
        transform.position = startPosition;
    }

    public void Spawner(Spawner spawner)
    {
        this.spawner = spawner;
    }


    public virtual void OnTriggerExit(Collider other)
    {
        if (ReturnToSpawner(other.gameObject.tag) == false)
        {
            Destroy(gameObject);
        }
    }

    public virtual void OnCollisionEnter(Collision collision)
    {

        if (ReturnToSpawner(collision.gameObject.tag) == false)
        {
            Destroy(gameObject);
        }
    }

    private void OnBecameInvisible()
    {
        if (gameObject.activeSelf)
            StartCoroutine(InvisibleTimer());
    }

    private void OnBecameVisible()
    {
        if (gameObject.activeSelf)
            StopCoroutine(InvisibleTimer());
    }

    public IEnumerator InvisibleTimer()
    {
        yield return new WaitForSeconds(invisibleLifetime);

        if (TryGetComponent(out Renderer renderer))
        {
            if (renderer.isVisible == false)
            {
                Debug.Log("Destroyed by InvisibleTimer");
                if(ReturnToSpawner("Untagged") ==false)
                {
                    Destroy(gameObject);
                }
            }
        }
    }

    public virtual bool ReturnToSpawner(string destroyedBy)
    {
        if (PlayerController.GetInstance().playerObject.CompareTag(destroyedBy))
        {
            PlayerController.GetInstance().GiveReward(reward);
        }
        if (spawner != null)
            return spawner.Return(gameObject, destroyedBy);
        else
            return false;
    }



}


